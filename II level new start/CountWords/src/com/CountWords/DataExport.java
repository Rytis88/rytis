package com.CountWords;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class DataExport {

	private final String START_WITH_A_G = "A_G.txt";
	private final String START_WITH_H_N = "H_N.txt";
	private final String START_WITH_O_U = "O_U.txt";
	private final String START_WITH_V_Z = "V_Z.txt";

	private Map<String, Integer> mapA_G = new TreeMap<>();
	private Map<String, Integer> mapH_N = new TreeMap<>();
	private Map<String, Integer> mapO_U = new TreeMap<>();
	private Map<String, Integer> mapV_Z = new TreeMap<>();
	
	private String fileOutputPath;

	public void print() throws IOException {
		printToFile(START_WITH_A_G, mapA_G);
		printToFile(START_WITH_H_N, mapH_N);
		printToFile(START_WITH_O_U, mapO_U);
		printToFile(START_WITH_V_Z, mapV_Z);
	}

	void selectMap(Map<String, Integer> words) throws IOException {
				
		Iterator<Map.Entry<String, Integer>> it = words.entrySet().iterator();
		while (it.hasNext()) {
		    Map.Entry<String, Integer> entry = it.next();
		    String word = SelectWords(entry.getKey());
			
			if (word == null) {
				continue;
			}
			
			switch (word) {

			case "a_g":
				mapA_G.put(entry.getKey(), entry.getValue());
				break;
			case "h_n":
				mapH_N.put(entry.getKey(), entry.getValue());
				break;
			case "o_u":
				mapO_U.put(entry.getKey(), entry.getValue());
				break;
			case "v_z":
				mapV_Z.put(entry.getKey(), entry.getValue());
				break;

			}
		}
		print();

	}

	private String SelectWords(String entry) {

		if (entry.matches("^[a-gA-G][a-zA-Z0-9]*$")) {

			return "a_g";
		} else if (entry.matches("^[h-nH-N][a-zA-Z0-9]*$")) {

			return "h_n";
		} else if (entry.matches("^[o-uO-U][a-zA-Z0-9]*$")) {

			return "o_u";
		} else if (entry.matches("^[v-zV-Z][a-zA-Z0-9]*$")) {

			return "v_z";
		}

		return null;
	}

	private void printToFile(String fileName, Map<String, Integer> words) throws IOException {
		File outputFile = new File(fileOutputPath + fileName);

		try {
			FileOutputStream is = new FileOutputStream(outputFile);
			OutputStreamWriter osw = new OutputStreamWriter(is);
			BufferedWriter w = new BufferedWriter(osw);
			for (Map.Entry<String, Integer> entry : words.entrySet()) {
				w.write(entry.getKey() + " = " + entry.getValue());
				w.newLine();
			}
			w.close();

		} catch (IOException e) {
			System.out.println("Error" + e.getMessage());
		} 
	}

}
