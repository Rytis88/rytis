package com.CountWords;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ListFiles extends Thread {

	private final String WHITESPACE = "\\s+";

	private List<String> listOneFile = new ArrayList<>();

	protected static List<String> listAllFile = Collections.synchronizedList(new ArrayList<>());
	protected static Map<String, Integer> map = new TreeMap<>();
	File file;
	BufferedReader br = null;

	public ListFiles(File file) {
		this.file = file;
	}

	public void run() {

		try {
			br = new BufferedReader(new FileReader(file));
			read(br);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private synchronized void read(BufferedReader br2) {

		String string;

		try {
			while ((string = br2.readLine()) != null) {
				listOneFile = Arrays.asList(extractLine(string));
				addList(listOneFile);
			}
			
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	private synchronized void addList(List<String> listOneFile2) {

		listAllFile.addAll(listOneFile);
	}

	public static void countWords(List<String> list) {

		for (int i = 0; i < list.size()-1; i++) {
			int num = 1;
			for (int j = i + 1; j < list.size(); j++) {
				if (list.get(i).toLowerCase().equals(list.get(j).toLowerCase())) {
					num++;
				}
			}
			
				if (!map.containsKey(list.get(i).toLowerCase()) && !list.get(i).isEmpty()) {
				map.put(list.get(i).toLowerCase(), num);
			}
		}

		try {
			DataExport dataExport = new DataExport();
			dataExport.selectMap(map);
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public String[] extractLine(String line) {
		
		line = line.replace("\"", "");
		line = line.replace("(", "");
		line = line.replace(",", "");
		line = line.replace(".", "");
		line = line.replace("-", "");
		line = line.replace("-", "");
		line = line.replace(")", "");
		String[] tokens = line.split(WHITESPACE);
		return tokens;
	}

}
