package lt.codeacademy.finansai.rs;
import java.util.Date;

public class IslaiduIrasas {
	private int id;
	private int kategorija;
	private double suma;
	private Date date;
	private int pozymis;
	public static final int MAX_ISLAIDU = 1000;

	static IslaiduIrasas[] masyvas = new IslaiduIrasas[MAX_ISLAIDU];
	
	public IslaiduIrasas(int id, int kategorija, double suma, Date date, int pozymis) {
		this.id = id;
		this.kategorija = kategorija;
		this.suma = suma;
		this.pozymis = pozymis;
		this.date = date;
			}
	public IslaiduIrasas() {
		
	}
	public double getSuma() {
		return suma;
	}
	public int getKategorija() {
		return kategorija;
	}
	
	public int getPozymis() {
		return pozymis;
	}
	public Date getDate() {
		return date;
	}
	
	public IslaiduIrasas[] visosIslaidos () {
		return masyvas;
	}
	
	
	

}
