package lt.codeacademy.finansai.rs;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Islaidos implements Print {

	Data data;
	Scanner scan = new Scanner(System.in);
	
	static double sumaIslaidos = 0;
	Meniu meniu;
	IslaiduIrasas islaiduIrasas;
	PasirinkimuKategorijaIslaidoms pasirinkimuKategorijaIslaidoms;

	public void ivestosIslaidos1() throws ParseException, MyException {
		int pasirinkimas2;
		boolean arTeisngasPasirenkimas2;
		IslaiduIrasas islaiduIrasas1 = new IslaiduIrasas();
		IslaiduIrasas[] perziuraIvestuIslaidu = islaiduIrasas1.visosIslaidos();

		if (perziuraIvestuIslaidu[0] == null) {
			p("Šiuo metu įvestų išlaidų nėra.");
			p("");
			System.out.println("Jeigu norite grįžti į pasirinkimo meniu, spauskite 1.");
			p("Jeigu norite įvesti išlaidas, spauskite 2.");
			int pasirinkimas3 = scan.nextInt();
			boolean arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2;

			while (!arTeisngasPasirenkimas3) {
				pasirinkimas3 = scan.nextInt();
				arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2;
			}

			if (pasirinkimas3 == 1) {
				Meniu meniu1 = new Meniu();
				meniu1.perziūrėtiDuomenis();
			} else if (pasirinkimas3 == 2) {
				ivestiIslaidas();
			}
		} else {

			do {
				p("Peržiūrėti visas įvestas išlaidas, spauskite 1.");
				p("Peržiūrėti atskiras įvestų išlaidų kategorijas, spauskite 2.");
				pasirinkimas2 = scan.nextInt();
				arTeisngasPasirenkimas2 = pasirinkimas2 == 1 || pasirinkimas2 == 2;
				if (!arTeisngasPasirenkimas2) {

					System.out.println();
					p("Įvedėte neteisingą skaičių.");

				}
			} while (!arTeisngasPasirenkimas2);

			switch (pasirinkimas2) {
			case 1:
				ivestosIslaidos();
				break;
			case 2:
				ivestosIslaidosKategorijomis();
				break;

			}
		}
	}

	public void ivestosIslaidosKategorijomis() throws ParseException, MyException {
		String kategorija1 = "";
		String kategorija2 = "";
		int sk = 0;
		double sumaIs = 0;
		String pozymis1 = "";
		IslaiduIrasas islaiduIrasas1 = new IslaiduIrasas();
		IslaiduIrasas[] perziuraIvestuIslaidu = islaiduIrasas1.visosIslaidos();
		PasirinkimuKategorijaIslaidoms pasirinkimuKategorijaIslaidoms1 = new PasirinkimuKategorijaIslaidoms();
		Integer kategorija = pasirinkimuKategorijaIslaidoms1.pasirinktiIslaiduKategorija();

		for (int i = 0; i < perziuraIvestuIslaidu.length; i++) {

			if (perziuraIvestuIslaidu[i] != null) {

				Integer kategorijaI = Integer.valueOf(perziuraIvestuIslaidu[i].getKategorija());
				Integer pozymis = perziuraIvestuIslaidu[i].getPozymis();
				switch (pozymis) {
				case 1:
					pozymis1 = "grynais.";
					break;
				case 2:
					pozymis1 = "banko kortele.";
				}

				switch (kategorijaI) {
				case 1:
					kategorija1 = "\"Prekybos centras\"";
					break;
				case 2:
					kategorija1 = "\"Būstas\"";
					break;
				case 3:
					kategorija1 = "\"Mokslai\"";
					break;
				case 4:
					kategorija1 = "\"Pramogos\"";
					break;
				case 5:
					kategorija1 = "\"Automobilis\"";
					break;
				case 6:
					kategorija1 = "\"Apranga\"";
					break;
				case 7:
					kategorija1 = "\"Kitos išlaidos\"";
					break;

				}
				switch (kategorija) {
				case 1:
					kategorija2 = "\"Prekybos centras\"";
					break;
				case 2:
					kategorija2 = "\"Būstas\"";
					break;
				case 3:
					kategorija2 = "\"Mokslai\"";
					break;
				case 4:
					kategorija2 = "\"Pramogos\"";
					break;
				case 5:
					kategorija2 = "\"Automobilis\"";
					break;
				case 6:
					kategorija2 = "\"Apranga\"";
					break;
				case 7:
					kategorija2 = "\"Kitos išlaidos\"";
					break;

				}
				if (kategorija == kategorijaI) {
					sk += 1;
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					String data = sdf.format(perziuraIvestuIslaidu[i].getDate());
					System.out.format(
							"Išlaidos " + (i + 1) + "Nr. įvesta " + data + ". Kategorija: " + kategorija1
									+ ". Išleista suma: %.2f eu." + " Pinigai išleisti " + pozymis1 + "\n",
							perziuraIvestuIslaidu[i].getSuma());
					sumaIs += perziuraIvestuIslaidu[i].getSuma();
				}

			}
		}
		if (sk > 1) {
			System.out.format("Kategorija " + kategorija1 + " iš viso įvesta išlaidų: %.2f eu.\n", sumaIs);
		}
		if (sk == 0) {
			p("Šiuo metu įvestų išlaidų iš kategorijos " + kategorija2 + " nėra.");

		}
		p("");
		p("Jeigu norite grįžti į pagrindinį meniu, spauskite 1.");
		p("Jeigu norite peržiūrėti visas įvestas išlaidas, spauskite 2.");
		p("Jeigu norite uždaryti programą \"Biudžetas\", spauskite 3.");

		int pasirinkimas3 = scan.nextInt();
		boolean arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2 || pasirinkimas3 == 3;

		while (!arTeisngasPasirenkimas3) {
			pasirinkimas3 = scan.nextInt();
			arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2 || pasirinkimas3 == 3;
		}

		if (pasirinkimas3 == 1) {
			Meniu meniu2 = new Meniu();
			meniu2.pagrindinisMeniu();

		} else if (pasirinkimas3 == 2) {
			ivestosIslaidos();
		} else if (pasirinkimas3 == 3) {
			Meniu meniu1 = new Meniu();
			meniu1.uzdarytiPrograma();
		}

	}

	public void ivestosIslaidos() throws ParseException, MyException {
		String kategorija1 = "";
		String pozymis1 = "";
		double sumaIs = 0;
		IslaiduIrasas islaiduIrasas1 = new IslaiduIrasas();
		IslaiduIrasas[] perziuraIvestuIslaidu = islaiduIrasas1.visosIslaidos();

		if (perziuraIvestuIslaidu.length > 0) {
			for (int i = 0; i < perziuraIvestuIslaidu.length; i++) {

				if (perziuraIvestuIslaidu[i] != null) {
					Integer kategorija = Integer.valueOf(perziuraIvestuIslaidu[i].getKategorija());
					Integer pozymis = perziuraIvestuIslaidu[i].getPozymis();
					switch (pozymis) {
					case 1:
						pozymis1 = "grynais.";
						break;
					case 2:
						pozymis1 = "banko kortele.";
					}
					switch (kategorija) {
					case 1:
						kategorija1 = "\"Prekybos centras\"";
						break;
					case 2:
						kategorija1 = "\"Būstas\"";
						break;
					case 3:
						kategorija1 = "\"Mokslai\"";
						break;
					case 4:
						kategorija1 = "\"Pramogos\"";
						break;
					case 5:
						kategorija1 = "\"Automobilis\"";
						break;
					case 6:
						kategorija1 = "\"Apranga\"";
						break;
					case 7:
						kategorija1 = "\"Kitos išlaidos\"";
						break;

					}
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					String data = sdf.format(perziuraIvestuIslaidu[i].getDate());

					System.out.format(
							"Išlaidos " + (i + 1) + "Nr. įvesta " + data + ". Kategorija: " + kategorija1
									+ ". Išleista suma: %.2f eu." + " Pinigai išleisti " + pozymis1 + "\n",
							perziuraIvestuIslaidu[i].getSuma());
					sumaIs += perziuraIvestuIslaidu[i].getSuma();
				}
			}
			System.out.format("Iš viso įvesta išlaidų: %.2f eu.\n", sumaIs);
			p("");
			p("Jeigu norite grįžti į pasirinkimo meniu, spauskite 1.");
			p("Jeigu norite įvesti išlaidas, spauskite 2.");
			int pasirinkimas3 = scan.nextInt();
			boolean arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2;

			while (!arTeisngasPasirenkimas3) {
				pasirinkimas3 = scan.nextInt();
				arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2;
			}

			if (pasirinkimas3 == 1) {
				Meniu meniu1 = new Meniu();
				meniu1.perziūrėtiDuomenis();
			} else {
				ivestiIslaidas();
			}

		}

	}

	public void ivestiIslaidas() throws ParseException, MyException {
		PasirinkimuKategorijaIslaidoms a = new PasirinkimuKategorijaIslaidoms();
		int kategorija = a.pasirinktiIslaiduKategorija();

		SumaPozymis sumaDataPozymis1 = new SumaPozymis();
		double suma = sumaDataPozymis1.suma1();
		int pozymis = sumaDataPozymis1.pozymis();
		sumaIslaidos += suma;
		Data data1 = new Data();
		Date date = data1.date1();
		IslaiduIrasas islaiduIrasas1 = new IslaiduIrasas();
		IslaiduIrasas[] masyvas = islaiduIrasas1.masyvas;

		for (int i = 0; i < masyvas.length; i++) {
			if (masyvas[i] == null) {
				masyvas[i] = new IslaiduIrasas(i + 1, kategorija, suma, date, pozymis);
				break;
			}
		}
		p("Įvedimas išsaugotas.\n");
		p("Jeigu norite grįžti į pagrindinį meniu, spauskite 1.");
		p("Jeigu norite įvesti naujas išlaidas, spauskite 2.");
		p("Jeigu norite peržiūrėti įvestas išlaidas spauskite 3.");
		int pasirinkimas3 = scan.nextInt();
		boolean arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2 || pasirinkimas3 == 3;

		while (!arTeisngasPasirenkimas3) {
			pasirinkimas3 = scan.nextInt();
			arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2 || pasirinkimas3 == 3;
		}

		switch (pasirinkimas3) {
		case 1:
			Meniu meniu1 = new Meniu();
			meniu1.pagrindinisMeniu();
			break;
		case 2:
			ivestiIslaidas();
			break;
		case 3:
			ivestosIslaidos1();
		}
	}

}
