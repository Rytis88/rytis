package lt.codeacademy.finansai.rs;
import java.util.Date;

public class PajamuIrasas {

	private int kategorija;
	private double suma;
	private Date date;
	protected int id;
	public static final int MAX_PAJAMU = 1000;
	

	static PajamuIrasas[] masyvas = new PajamuIrasas[MAX_PAJAMU];
	
	public PajamuIrasas() {
		
	}

	public PajamuIrasas(int id, int kategorija, double suma, Date date) {

		this.kategorija = kategorija;
		this.suma = suma;
		this.date = date;
		this.id = id;
	}
	

	public double getSuma() {
		return suma;
	}
	public int getId() {
		return id;
	}

	public int getKategorija() {
		return kategorija;
	}

	public Date getDate() {
		return date;
	}
	public PajamuIrasas[] visosPajamos () {
		return masyvas;
	}

}
