package lt.codeacademy.finansai.rs;
import java.text.ParseException;
import java.util.Scanner;

public class Balansas implements Print {

	Scanner scan = new Scanner(System.in);
	boolean arTeisngasPasirenkimas;
	int pasirinkimas;

	public void balansas() throws ParseException, MyException {
		Islaidos m = new Islaidos();
		double sumaIslaidos1 = m.sumaIslaidos;

		Meniu t = new Meniu();
		double sumaPajamos1 = t.sumaPajamos;

		double balansas = sumaPajamos1 - sumaIslaidos1;

		System.out.format("Jūsų sąskaitos balansas: %.2f eu.\n", balansas);
		do {
			p("Jeigu norite grįžti į pagrindinį meniu, spauskite 1.");
			p("Jeigu norite uždaryti programą, spauskite 2.");

			pasirinkimas = scan.nextInt();
			arTeisngasPasirenkimas = (pasirinkimas == 1 || pasirinkimas == 2);
			if (!arTeisngasPasirenkimas) {
				p("\n");
				p("Įvedėte neteisingą skaičių.");
				p("");
			}

		} while (!arTeisngasPasirenkimas);

		switch (pasirinkimas) {
		case 1:
			Meniu meniu1 = new Meniu();
			meniu1.pagrindinisMeniu();
			break;
		case 2:
			Meniu meniu2 = new Meniu();
			meniu2.uzdarytiPrograma();
			break;

		}

	}

}
