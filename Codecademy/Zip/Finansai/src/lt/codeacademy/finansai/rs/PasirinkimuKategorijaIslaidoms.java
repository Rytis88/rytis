package lt.codeacademy.finansai.rs;
import java.util.Scanner;

public class PasirinkimuKategorijaIslaidoms implements Print {

	Scanner scan = new Scanner(System.in);

	public int pasirinktiIslaiduKategorija() {
		
		boolean arTeisngasPasirenkimas = true;
		int pasirinkimas1;
		
		
		System.out.println("Pasirinkite kategoriją: ");
		
		do {
			p("1. Prekybos centras");
			p("2. Būstas");
			p("3. Mokslai");
			p("4. Pramogos");
			p("5. Automobilis");
			p("6. Apranga");
			p("7. Kita");
		
		pasirinkimas1 = scan.nextInt();
		
		arTeisngasPasirenkimas = pasirinkimas1 == 1 || pasirinkimas1 == 2 || pasirinkimas1 == 3 || pasirinkimas1 == 4 || pasirinkimas1 == 5 || pasirinkimas1 == 6 || pasirinkimas1 == 7;
			
		if (!arTeisngasPasirenkimas) {
			p("\n");
			p("Įvedėte neteisingą skaičių.");
			p("Iveskite skaičių, kurią kategoriją norite pasirinkti: ");
			
		}
		
		
	} while (!arTeisngasPasirenkimas);
		
	return 	pasirinkimas1;

}


}
