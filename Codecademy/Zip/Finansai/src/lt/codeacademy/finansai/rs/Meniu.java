package lt.codeacademy.finansai.rs;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Meniu implements Print {

	public static final int MAX_PAJAMU = 1000;

	private PajamuIrasas[] masyvas = new PajamuIrasas[MAX_PAJAMU];

	
	static double sumaPajamos = 0;

	Scanner scan = new Scanner(System.in);

	public void Pasisveikinimas() {
		p("***************************************************");
		p("*Sveiki. Programa \"Biudžetas\" pasiruošusi darbui. *");
		p("***************************************************");
		p("");
	}

	public void pagrindinisMeniu() throws ParseException, MyException {

		boolean arTeisngasPasirenkimas;
		int pasirinkimas;

		do {
			p("***************************************************");
			p("*               Pagrindinis meniu                 *");
			p("*                                                 *");
			p("*  Jeigu norite peržiūrėti duomenis, spauskite 1. *");
			p("*    Jeigu norite įvesti duomenis, spauskite 2.   *");
			p("*  Jeigu norite peržiūrėti balansą, spauskite 3.  *");
			p("*   Jeigu norite uždaryti programą, spauskite 4.  *");
			p("***************************************************");
			pasirinkimas = scan.nextInt();
			arTeisngasPasirenkimas = (pasirinkimas == 1 || pasirinkimas == 2 || pasirinkimas == 3 || pasirinkimas == 4);
			if (!arTeisngasPasirenkimas) {
				p("\n");
				p("Įvedėte neteisingą skaičių.");
				p("");
			}

		} while (!arTeisngasPasirenkimas);

		switch (pasirinkimas) {
		case 1:
			perziūrėtiDuomenis();
			break;
		case 2:
			ivestiDuomenis();
			break;
		case 3:
			Balansas balansas1 = new Balansas();
			balansas1.balansas();
			break;
		case 4:
			uzdarytiPrograma();
			break;

		}

	}

	public void perziūrėtiDuomenis() throws ParseException, MyException {

		int pasirinkimas2;
		boolean arTeisngasPasirenkimas2;

		do {

			System.out.println();
			p("Jeigu norite peržiūrėti įvestas pajamas, spauskite 1.");
			p("Jeigu norite peržiūrėti įvestas išlaidas, spauskite 2.");
			p("Jeigu norite peržiūrėti balansą, spauskite 3.");
			p("Jeigu norite grįžti į pagrindinį meniu, spauskite 4.");
			pasirinkimas2 = scan.nextInt();
			arTeisngasPasirenkimas2 = pasirinkimas2 == 1 || pasirinkimas2 == 2 || pasirinkimas2 == 3
					|| pasirinkimas2 == 4;
			if (!arTeisngasPasirenkimas2) {

				System.out.println();
				p("Įvedėte neteisingą skaičių.");

			}
		} while (!arTeisngasPasirenkimas2);

		switch (pasirinkimas2) {
		case 1:
			ivestosPajamos1();
			break;
		case 2:
			ivestosIslaidos();
			break;
		case 3:
			Balansas balansas1 = new Balansas();
			balansas1.balansas();
			break;
		case 4:
			pagrindinisMeniu();
			break;
		}
	}

	public void ivestosPajamos1() throws ParseException, MyException {
		int pasirinkimas2;
		boolean arTeisngasPasirenkimas2;
		PajamuIrasas pajamuIrasas1 = new PajamuIrasas();
		PajamuIrasas[] perziuraIvestuPajamu = pajamuIrasas1.visosPajamos();

		if (perziuraIvestuPajamu[0] == null) {
			p("Šiuo metu įvestų pajamų nėra.");
			p("");
			System.out.println("Jeigu norite grįžti į pasirinkimo meniu, spauskite 1.");
			p("Jeigu norite įvesti pajamas, spauskite 2.");
			int pasirinkimas3 = scan.nextInt();
			boolean arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2;

			while (!arTeisngasPasirenkimas3) {
				pasirinkimas3 = scan.nextInt();
				arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2;
			}

			if (pasirinkimas3 == 1) {
				perziūrėtiDuomenis();
			} else if (pasirinkimas3 == 2) {
				ivestiPajamas();
			}
		} else {

			do {
				p("Peržiūrėti visas įvestas pajamas, spauskite 1.");
				p("Peržiūrėti atskiras įvestų pajamų kategorijas, spauskite 2.");
				pasirinkimas2 = scan.nextInt();
				arTeisngasPasirenkimas2 = pasirinkimas2 == 1 || pasirinkimas2 == 2;
				if (!arTeisngasPasirenkimas2) {

					System.out.println();
					p("Įvedėte neteisingą skaičių.");

				}
			} while (!arTeisngasPasirenkimas2);

			switch (pasirinkimas2) {
			case 1:
				ivestosPajamos();
				break;
			case 2:
				ivestosPajamosKategorijomis();
				break;

			}
		}
	}

	public void ivestosPajamosKategorijomis() throws ParseException, MyException {
		String kategorija1 = "";
		String kategorija2 = "";
		int sk = 0;
		double suma = 0;
		PajamuIrasas pajamuIrasas1 = new PajamuIrasas();
		PajamuIrasas[] perziuraIvestuPajamu = pajamuIrasas1.visosPajamos();
		PasirinkimuKategorija pasirinkimuKategorija1 = new PasirinkimuKategorija();
		Integer kategorija = pasirinkimuKategorija1.pasirinktiPajamuKategorija();

		for (int i = 0; i < perziuraIvestuPajamu.length; i++) {

			if (perziuraIvestuPajamu[i] != null) {

				Integer kategorijaI = Integer.valueOf(perziuraIvestuPajamu[i].getKategorija());

				switch (kategorijaI) {
				case 1:
					kategorija1 = "\"Alga\"";
					break;
				case 2:
					kategorija1 = "\"Nuoma\"";
					break;
				case 3:
					kategorija1 = "\"Stipendija\"";
					break;
				case 4:
					kategorija1 = "\"Pašalpa\"";
					break;
				case 5:
					kategorija1 = "\"Individuali veikla\"";
					break;
				case 6:
					kategorija1 = "\"Dovanos\"";
					break;
				case 7:
					kategorija1 = "\"Kitos pajamos\"";
					break;
				}
				switch (kategorija) {
				case 1:
					kategorija2 = "\"Alga\"";
					break;
				case 2:
					kategorija2 = "\"Nuoma\"";
					break;
				case 3:
					kategorija2 = "\"Stipendija\"";
					break;
				case 4:
					kategorija2 = "\"Pašalpa\"";
					break;
				case 5:
					kategorija2 = "\"Individuali veikla\"";
					break;
				case 6:
					kategorija2 = "\"Dovanos\"";
					break;
				case 7:
					kategorija2 = "\"Kitos pajamos\"";
					break;
				}
				if (kategorija == kategorijaI) {
					sk += 1;
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					String data = sdf.format(perziuraIvestuPajamu[i].getDate());
					System.out.format("Nr" + (i + 1) + ". įvesta " + data + ". Kategorija: " + kategorija1
							+ ". Gauta suma: %.2f eu.\n", perziuraIvestuPajamu[i].getSuma());
					suma += perziuraIvestuPajamu[i].getSuma();
				}

			}
		}
		if (sk > 1) {
			System.out.format("Kategorija " + kategorija1 + " iš viso įvesta pajamų: %.2f eu.\n", suma);
		}
		if (sk == 0) {
			p("Šiuo metu įvestų pajamų iš kategorijos " + kategorija2 + " nėra.");

		}
		p("");
		p("Jeigu norite grįžti į pagrindinį meniu, spauskite 1.");
		p("Jeigu norite peržiūrėti visas įvestas pajamas, spauskite 2.");
		p("Jeigu norite uždaryti programą \"Biudžetas\", spauskite 3.");

		int pasirinkimas3 = scan.nextInt();
		boolean arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2 || pasirinkimas3 == 3;

		while (!arTeisngasPasirenkimas3) {
			pasirinkimas3 = scan.nextInt();
			arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2 || pasirinkimas3 == 3;
		}

		if (pasirinkimas3 == 1) {
			pagrindinisMeniu();

		} else if (pasirinkimas3 == 2) {
			ivestosPajamos();
		} else if (pasirinkimas3 == 3) {
			uzdarytiPrograma();
		}

	}

	public void ivestosPajamos() throws ParseException, MyException {
		String kategorija1 = "";
		PajamuIrasas pajamuIrasas1 = new PajamuIrasas();
		PajamuIrasas[] perziuraIvestuPajamu = pajamuIrasas1.visosPajamos();
		double sumaPa = 0;

		if (perziuraIvestuPajamu.length > 0) {
			for (int i = 0; i < perziuraIvestuPajamu.length; i++) {

				if (perziuraIvestuPajamu[i] != null) {
					Integer kategorija = Integer.valueOf(perziuraIvestuPajamu[i].getKategorija());
					switch (kategorija) {
					case 1:
						kategorija1 = "\"Alga\"";
						break;
					case 2:
						kategorija1 = "\"Nuoma\"";
						break;
					case 3:
						kategorija1 = "\"Stipendija\"";
						break;
					case 4:
						kategorija1 = "\"Pašalpa\"";
						break;
					case 5:
						kategorija1 = "\"Individuali veikla\"";
						break;
					case 6:
						kategorija1 = "\"Dovanos\"";
						break;
					case 7:
						kategorija1 = "\"Kitos pajamos\"";
						break;

					}
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					String data = sdf.format(perziuraIvestuPajamu[i].getDate());
					System.out.format("Nr" + (i + 1) + ". įvesta " + data + " Kategorija: " + kategorija1
							+ ". Gauta suma: %.2feu.\n", perziuraIvestuPajamu[i].getSuma());
					sumaPa += perziuraIvestuPajamu[i].getSuma();
				}
			}
			System.out.format("Iš viso įvesta pajamų: %.2f eu.\n", sumaPa);
			p("");
			p("Jeigu norite grįžti į pasirinkimo meniu, spauskite 1.");
			p("Jeigu norite įvesti pajamas, spauskite 2.");
			int pasirinkimas3 = scan.nextInt();
			boolean arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2;

			while (!arTeisngasPasirenkimas3) {
				pasirinkimas3 = scan.nextInt();
				arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2;
			}

			if (pasirinkimas3 == 1) {
				perziūrėtiDuomenis();
			} else {
				ivestiPajamas();
			}

		}

	}

	public void ivestosIslaidos() throws ParseException, MyException {
		Islaidos islaidos1 = new Islaidos();
		islaidos1.ivestosIslaidos1();
	}

	public void ivestiDuomenis() throws ParseException, MyException {

		int pasirinkimas2;
		boolean arTeisngasPasirenkimas2;

		do {

			p("Jeigu norite įvesti pajamas, spauskite 1.");
			p("Jeigu norite įvesti išlaidas, spauskite 2.");
			p("Jeigu norite grįžti į pagrindinį meniu, spauskite 3.");
			pasirinkimas2 = scan.nextInt();
			arTeisngasPasirenkimas2 = pasirinkimas2 == 1 || pasirinkimas2 == 2 || pasirinkimas2 == 3;
			if (!arTeisngasPasirenkimas2) {
				System.out.println();
				p("Įvedėte neteisingą skaičių.");
			}
		} while (!arTeisngasPasirenkimas2);

		switch (pasirinkimas2) {
		case 1:
			ivestiPajamas();
			break;
		case 2:
			ivestiIslaidas();
			break;
		case 3:
			pagrindinisMeniu();
			break;
		}
	}

	public void ivestiPajamas() throws ParseException, MyException {
		PasirinkimuKategorija a = new PasirinkimuKategorija();
		int kategorija = a.pasirinktiPajamuKategorija();

		SumaPozymis sumaDataPozymis1 = new SumaPozymis();
		double suma = sumaDataPozymis1.suma1();
		sumaPajamos += suma;
		Data data1 = new Data();
		Date date = data1.date1();
		PajamuIrasas pajamuIrasas1 = new PajamuIrasas();
		PajamuIrasas[] masyvas = pajamuIrasas1.masyvas;

		for (int i = 0; i < masyvas.length; i++) {
			if (masyvas[i] == null) {
				masyvas[i] = new PajamuIrasas(i + 1, kategorija, suma, date);
				break;
			}

		}

		p("Įvedimas išsaugotas.\n");
		p("Jeigu norite grįžti į pagrindinį meniu, spauskite 1.");
		p("Jeigu norite įvesti naujas pajamas, spauskite 2.");
		p("Jeigu norite peržiūrėti įvestas pajamas spauskite 3.");
		int pasirinkimas3 = scan.nextInt();
		boolean arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2 || pasirinkimas3 == 3;

		while (!arTeisngasPasirenkimas3) {
			pasirinkimas3 = scan.nextInt();
			arTeisngasPasirenkimas3 = pasirinkimas3 == 1 || pasirinkimas3 == 2 || pasirinkimas3 == 3;
		}

		switch (pasirinkimas3) {
		case 1:
			pagrindinisMeniu();
			break;
		case 2:
			ivestiPajamas();
			break;
		case 3:
			ivestosPajamos1();
		}
	}

	public void ivestiIslaidas() throws ParseException, MyException {
		Islaidos islaidos1 = new Islaidos();
		islaidos1.ivestiIslaidas();
	}

	public void uzdarytiPrograma() {
		p("Programa \"Biudžetas\" baigė darbą. Geros dienos!");
		System.exit(0);
	}

	public static void main(String[] args) {
		Meniu meniu1 = new Meniu();
		meniu1.Pasisveikinimas();
		try {
			meniu1.pagrindinisMeniu();
		} catch (ParseException | MyException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}

	}

}
